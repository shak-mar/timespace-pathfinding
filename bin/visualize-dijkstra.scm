(import
    (lib base-objects)
    (lib board)
    (lib dijkstra)
    (lib graph)
    (lib objects)
    (lib visualization)
    (only (srfi :1) concatenate)
    (rnrs))

(define touched-points (list))
(define (touch-node! n)
    (let ((point (cons (ca n 'x 'raw) (ca n 'y 'raw))))
        (if (member point touched-points)
            #f
            (begin
                (display (length touched-points))
                (display " points touched:") (newline)
                (set! touched-points (cons point touched-points))
                (for-each (lambda (l) (display l) (newline)) (auto-plot-points touched-points))))))

(define (taint-relat object)
    (lambda (arg)
        (if (eq? arg 'node)
            (taint-node (object arg))
            (object arg))))

(define (taint-relats object)
    (ca object 'map taint-relat))

(define (taint-node object)
    (lambda (arg)
        (touch-node! object)
        (if (eq? arg 'relations)
            (taint-relats (object arg))
            (object arg))))

(define (points-of nodes)
    (map (lambda (n) (cons (ca n 'x 'raw) (ca n 'y 'raw))) nodes))

(let* ((the-board (ca board 'two-dimensional 20 20))
       (center (ca the-board 'node-at 19 19))
       (is-x-0? (lambda (node) (and (= 0 (ca node 'y 'raw)) (= 0 (ca node 'x 'raw))))))
       (let ((path (dijkstra (taint-node center) is-x-0?)))
        (display "The path:") (newline)
        (for-each (lambda (l) (display l) (newline)) (auto-plot-points (points-of path)))))
