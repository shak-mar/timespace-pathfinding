(import
    (lib a-star)
    (lib base-objects)
    (lib board)
    (lib dijkstra)
    (lib graph)
    (lib objects)
    (lib visualization)
    (only (srfi :1) concatenate)
    (rnrs))

(define touched-points (list))
(define (touch-node! n)
    (let ((point (cons (ca n 'x 'raw) (ca n 'y 'raw))))
        (if (member point touched-points)
            #f
            (begin
                (display (length touched-points))
                (display " points touched:") (newline)
                (set! touched-points (cons point touched-points))
                (for-each (lambda (l) (display l) (newline)) (auto-plot-points touched-points))))))

(define (taint-relat object)
    (lambda (arg)
        (if (eq? arg 'node)
            (taint-node (object arg))
            (object arg))))

(define (taint-relats object)
    (ca object 'map taint-relat))

(define (taint-node object)
    (lambda (arg)
        (touch-node! object)
        (if (eq? arg 'relations)
            (taint-relats (object arg))
            (object arg))))

(define (points-of nodes)
    (map (lambda (n) (cons (ca n 'x 'raw) (ca n 'y 'raw))) nodes))

(define (square n)
    (* n n))

(let* ((the-board (ca board 'two-dimensional 20 20))
       (center (ca the-board 'node-at 10 10))
       (heuristic (lambda (node)
            (+ (square (ca node 'x 'raw)) (square (ca node 'y 'raw))))))
       (let ((path (a-star (taint-node center) heuristic)))
        (display "The path:") (newline)
        (for-each (lambda (l) (display l) (newline)) (auto-plot-points (points-of path)))))
