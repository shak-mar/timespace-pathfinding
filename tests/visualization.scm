(library (tests visualization)
    (export run)
    (import
        (rnrs)
        (tests library)
        (lib utils)
        (lib visualization))

    (define (run)
        ;; PLOT-POINTS takes dimensions of the area to plot, and a list of
        ;; points, which are (x . y) tuples, X and Y counted from zero.
        (check (plot-points 2 2 '((0 . 0))) => '("* " "  "))
        (check (plot-points 2 2 '((0 . 1))) => '("  " "* "))
        (check (plot-points 4 4 (map (lambda (x) (cons x x)) (range-upper-exclusive 0 4)))
                => '("*   "
                    " *  "
                    "  * "
                    "   *"))

        ;; AUTO-PLOT-POINTS doesn't need the dimensions, and computes
        ;; the smalles possible dimensions first
        (check (auto-plot-points '()) => '())
        (check (auto-plot-points '((0 . 0))) => '("*"))
        (check (auto-plot-points '((2 . 2))) => '("   " "   " "  *"))))
