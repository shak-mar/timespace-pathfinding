(library (tests board)
    (export run)
    (import
        (rnrs)
        (tests library)
        (lib base-objects)
        (lib board)
        (lib graph)
        (lib objects)
        (srfi :26))

    (define (run)
        ;; (board 'two-dimensional) takes a width and a height and
        ;; constructs a board.  Boards respond to 'board? with a wrapped #t
        (check (ca board 'two-dimensional 10 10 'board? 'raw) => #t)
        ;; Width and height are accessible via 'width and 'height
        (check (ca board 'two-dimensional 1 2 'width 'raw) => 1)
        (check (ca board 'two-dimensional 1 2 'height 'raw) => 2)

        (let ((a-board (ca board 'two-dimensional 15 9)))
            ;; Using 'node-at with coordinates within the width and height,
            ;; you can access nodes at those coordinates
            (check-capable (ca a-board 'node-at 0 0) node)
            (check-capable (ca a-board 'node-at 7 7) node)
            ;; Nodes are not created outside of the board boundaries
            (check (eq? #t (ca a-board 'node-at 2 9 'node? 'raw)) => #f)
            ;; Nodes at different coordinates are different
            (check (node=? (ca a-board 'node-at 0 0) (ca a-board 'node-at 0 1)) => #f))

        ;; Nodes have 1-cost relations to their (non-diagonal) neighbours
        (let* ((a-board (ca board 'two-dimensional 3 3))
               (center (ca a-board 'node-at 1 1))
               (corner (ca a-board 'node-at 0 0))
               (edge (ca a-board 'node-at 0 1)))
            (check (ca center 'relations 'map (cut <> 'node) 'contains? edge 'raw) => #t)
            (check (ca center 'relations 'map (cut <> 'node) 'contains? corner 'raw) => #f)
            (ca edge 'relations 'map (cut <> 'node) 'map (lambda (neighbour)
                (check-capable neighbour node)))
            (check (ca edge 'relations 'map (cut ca <> 'cost 'raw) 'raw)
                => (list 1 1 1)))
))
