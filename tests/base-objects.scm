(library (tests base-objects)
    (export run)
    (import
        (rnrs)
        (srfi :26)
        (tests library)
        (prefix (lib check) lib:)
        (lib objects)
        (lib base-objects))

    ;; You can define capabilities using DEFINE-CAPABILITY like this
    (define-capability (can-increment trace object)
        (lib:check trace (ca object 'increment -1) => 0)
        (lib:check trace (ca object 'increment 5) => 6))

    (define (run)
        ;; CHECK-CAPABILITIES performs runtime checks of capabilities
        ;; an object reports.  (Capabilities are like interfaces.)
        ;; Objects that aren't capable of anything are valid
        (check-capabilities (store))
        (let ((good-object (store))
              (bad-object (store))
              (can-greet (store)))
            (ca good-object 'greet '= (wrap "Hey there!"))
            (ca good-object 'increment '= (lambda (x) (+ x 1)))
            ;; Objects can claim to support capabilities by responding to
            ;; 'capabilities with a wrapped list of them
            (ca bad-object 'capabilities '= (wrap (list can-greet)))
            (ca good-object 'capabilities '= (wrap (list can-greet can-increment)))
            ;; There's CAPABILITY and EQUATABLE capabilities that
            ;; capabilities must be capable of
            (ca can-greet 'capabilities '= (wrap (list capability equatable)))
            ;; Capabilities need a 'check procedure which takes a TRACE
            ;; string to pass to LIB:CHECK and an object (which claims
            ;; to be capable) to check
            (ca can-greet 'check '= (lambda (trace) (lambda (object)
                (lib:check trace (string? (ca object 'greet 'raw)) => #t))))
            ;; Capabilities must respond to 'name with a wrapped string
            (ca can-greet 'name '= (wrap "can-greet"))
            (ca can-greet '=? '= (lambda (other) (wrap (equal? other can-greet))))
            (check-capabilities can-greet)
            (check-capabilities good-object)
            ;; Using CHECK-CAPABLE, you can make sure an object is
            ;; capable of some capability
            (check-capable good-object can-greet)
            ;; If an object isn't capable of what it claims, a condition
            ;; is raised.
            (check (guard
                (_ (else 'caught))
                (check-capabilities bad-object))
                => 'caught))

        ;; WRAP wraps a value in an object which responds with it to 'raw
        (check (ca wrap 5 'raw) => 5)
        (check (ca wrap #f 'raw) => #f)
        ;; The wrapper will respond to 'wrapper? with a wrapped #t
        (check (ca wrap 5 'wrapper? 'raw) => #t)
        ;; It will also respond to '=? by testing for equality with
        ;; the object it then receives, resulting in a wrapped #t or #f
        (check (ca wrap 5 '=? (wrap 5) 'raw) => #t)
        (check (ca wrap 5 '=? (wrap 6) 'raw) => #f)
        (check (ca wrap 5 '=? 6 'raw) => #f)

        ;; Depending on the type of object, the wrapper responds to
        ;; extra messages:
        ; -- Numbers --
        ;; 'iota calls IOTA from SRFI-1
        (check (ca wrap 3 'iota 'raw) => '(0 1 2))
        ; -- Booleans --
        ;; 'and applies AND
        (check (ca wrap #t 'and (wrap #t) 'raw) => #t)
        (check (ca wrap #t 'and (wrap #f) 'raw) => #f)
        ; -- Lists --
        ;; 'contains? applies MEMP with '=?
        (check (ca wrap (list 1 2 3) 'map wrap 'contains? (wrap 1) 'raw) => #t)
        (check (ca wrap (list 1 2 3) 'map wrap 'contains? (wrap 5) 'raw) => #f)
        ;; 'map applies MAP
        (check (ca wrap (list 1 2 3) 'map (cut + <> 1) 'raw) => '(2 3 4))

        ;; LAZY defers evaluation of an object until it is used
        (check (eq?
                'jumped-out
                (call-with-current-continuation (lambda (c)
                    (lazy (begin
                        (c 'jumped-out)
                        (wrap 5))))))
            => #f)
       (check ((lazy (wrap 5)) 'raw) => 5)
))
