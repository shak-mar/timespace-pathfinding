(library (tests objects)
    (export run)
    (import
        (rnrs)
        (tests library)
        (lib objects))

    (define (%lazy thunk)
        (let ((the-object #f))
            (lambda (x)
                (or the-object (set! the-object (thunk)))
                (the-object x))))

    (define wrap (generic-new (lambda _ #f) (lambda (this $)
        (ca this 'raw '= $)
        (ca this 'wrapper? '= (%lazy (lambda _ (wrap #t))))
        (ca this '=? '= (lambda (other)
            (wrap (and (object? other)
                (equal? (this 'raw) (other 'raw))))))
        this)))

    (define (run)
        ;; CURRIED-APPLY applies a curried procedure to multiple arguments
        (check (curried-apply (lambda (x) (+ x 1)) 2) => 3)
        (check (curried-apply (lambda (x) (lambda (y) (+ x y))) 2 3) => 5)
        ;; CA is an alias for CURRIED-APPLY
        (check ca => curried-apply)

        ;; We dub calling a procedure sending a *message* to an *object*
        ;; yielding a *response*.

        ;; STORE creates an object which behaves as a mutable key-value
        ;; store.
        (let ((person (store)))
            ;; You can save things by sending (curriedly) the key,
            ;; '=, and the value.  Due to the nature of value retrieval,
            ;; you should only store procedures.  (Hence use WRAP)
            (ca person 'name '= (wrap "Moses"))
            ;; The assignment will respond with the assigned value
            (check (ca person 'age '= (wrap 4000) 'raw) => 4000)
            ;; You can change things you already stored
            (ca person 'age '= (wrap 4001))
            ;; Retrieve things by simply sending the key
            (check (ca person 'name 'raw) => "Moses")
            (check (ca person 'age 'raw) => 4001)
            ;; You can go multiple hops, because unknown keys will yield
            ;; new key-value store objects
            (ca person 'street 'number 'imaginary-part '= (wrap 12))
            (check (ca person 'street 'number 'imaginary-part 'raw) => 12))

        ;; You can use pretty much anything as keys
        (let ((object (store)))
            (ca object "attribute name" '= (wrap "value"))
            (check (ca object "attribute name" 'raw) => "value"))

        ;; You can also store non-object values, but you cannot change them
        (let ((object (store)))
            (ca object 'raw '= 5)
            (check (object 'raw) => 5))
            ;; This would fail trying to call non-procedure 6:
            ; (ca object 'raw '= 6)

        ;; NEW creates object with overrided behaviour on non-symbol messages
        (check ((new (lambda (a b) b)) 5) => 5)
        (check (equal? 'symbol ((new (lambda (a b) b)) 'symbol)) => #f)

        ;; The first argument NEW passes to the given procedure is a
        ;; new object for each call, also dubbed a *scope*, or the
        ;; *constructed* object.  *Constructors* are also called
        ;; *classes*.
        (let* ((construct-person (new (lambda (this $)
                   (ca this 'name '= $)
                   (ca this 'greet '= (new (lambda (scope $)
                       (ca $ 'print (string-append "Hello " (ca this 'name 'raw) "!")))))
                   this)))
               (person (construct-person (wrap "Moses")))
               (fake-io ((new (lambda (this $)
                   (ca this 'print '= (new (lambda (scope $) $)))
                   this)) #f)))
               (check (ca person 'greet fake-io) => "Hello Moses!"))

        ;; Using GENERIC-NEW, you can override which messages are
        ;; considered keys
        (let ((funky-object (generic-new number? (lambda (a b) b))))
            (ca funky-object 5 '= (wrap 'something))
            (check (ca funky-object 5 'raw) => 'something))

        ;; OBJECT? determines if its parameter is an object
        (check (object? (new (lambda (a b) #f))) => #t)
        (check (object? 5) => #f)
        ;; Since objects are simply procedures, all procedures are objects
        (check (object? (lambda (_) #f)) => #t)

        ;; COW creates a copy-on-write clone of an object
        (check ((cow (wrap 5)) 'raw) => 5)
        (let* ((the-store (store))
               (copy (cow the-store)))
            ;; Properties are stored separately in the original object and the
            ;; cloned object
            (ca the-store 'separate-property '= (wrap 5))
            (ca copy 'separate-property '= (wrap 6))
            (check (ca the-store 'separate-property 'raw) => 5)
            (check (ca copy 'separate-property 'raw) => 6)

            ;; Properties that are set in the original object appear in the
            ;; clone
            (ca the-store 'shared-property '= 'value)
            (check (copy 'shared-property) => 'value)

            ;; Deeply nested properties are shared between both objects though
            (ca the-store 'deeply 'nested 'property '= (wrap 5))
            (ca copy 'deeply 'nested 'property '= (wrap 6))
            (check (ca the-store 'deeply 'nested 'property 'raw) => 6)

            ;; You can also write properties that didn't exist in the original
            ;; object
            (ca copy 'new-property '= 'value)
            (check (copy 'new-property) => 'value))))
