#!/bin/sh

apk add git make gcc musl-dev --update-cache
git clone https://github.com/lecram/congif
cd congif
git am ../third-party/0001-Default-to-80x80-terminal-if-the-real-size-cannot-be.patch
make
