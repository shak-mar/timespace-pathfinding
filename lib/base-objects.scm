(library (lib base-objects)
    (export
        capability
        check-capabilities
        check-capable
        define-capability
        equatable
        lazy
        wrap)
    (import
        (rnrs)
        (lib check)
        (lib utils)
        (lib objects)
        (only (srfi :1) iota))

    (define (%check-capabilities expression object)
        (ca object 'capabilities 'map (lambda (capability)
            (%check-capable expression object capability))))

    (define-syntax check-capabilities
        (syntax-rules ()
            ((check-capabilities object)
             (%check-capabilities 'object object))))

    (define (%check-capable object-expr object capability)
        (define (->string x)
            (call-with-string-output-port (lambda (output-port)
                (display x output-port))))
        (let* ((object-str (->string object-expr))
               (trace (begin
                   (%check-capabilities
                       (string-append "a capability of " object-str)
                       capability)
                   (string-append
                       object-str
                       ", checking for "
                       (ca capability 'name 'raw)))))
            (ca capability 'check trace object)
            (check trace (ca object 'capabilities 'contains? capability 'raw) => #t)))

    (define-syntax check-capable
        (syntax-rules ()
            ((check-capable object capability)
             (%check-capable 'object object capability))))

    (define (%make-capability symbol proc)
        (let ((the-capability (%make-pseudo-capability symbol proc)))
            (ca the-capability 'capabilities '= (wrap (list equatable capability)))
            the-capability))

    (define-syntax define-capability
        (syntax-rules ()
            ((define-capability (capability trace object) . body)
             (define capability (%make-capability 'capability
                 (lambda (trace object) . body))))))

    (define (%lazy thunk)
        (let ((the-object #f))
            (lambda (x)
                (or the-object (set! the-object (thunk)))
                (the-object x))))

    (define-syntax lazy
        (syntax-rules ()
            ((lazy object)
             (%lazy (lambda _ object)))))

    (define wrap (generic-new (lambda _ #f) (lambda (this $)
        (ca this 'raw '= $)
        (ca this 'wrapper? '= (lazy (wrap #t)))
        (ca this '=? '= (lambda (other)
            (wrap (and (object? other)
                (equal? (this 'raw) (other 'raw))))))
        (cond ((number? (this 'raw))
               (ca this 'iota '= (lazy (wrap (iota (this 'raw))))))
              ((boolean? (this 'raw))
               (ca this 'and '= (lambda (other)
                   (wrap (and (this 'raw) (other 'raw))))))
              ((list? (this 'raw))
               (ca this 'contains? '= (lambda (object)
                   (wrap (and (memp
                       (lambda (our-object)
                           (eq? #t (ca object '=? our-object 'raw)))
                       (this 'raw)) #t))))
               (ca this 'map '= (lambda (proc)
                   (wrap (map proc (this 'raw)))))))
        this)))

    (define (%make-pseudo-capability symbol proc)
        (let ((the-capability (store)))
            (ca the-capability 'name '= (wrap (symbol->string symbol)))
            (ca the-capability 'check '= (lambda (trace) (lambda (object)
                (proc trace object))))
            (ca the-capability '=? '= (lambda (other)
                (wrap (equal? other the-capability))))
            the-capability))

    (define equatable (%make-pseudo-capability 'equatable (lambda (trace object)
        (check trace (ca object '=? (store) 'raw) => #f)
        (check trace (ca object '=? object 'raw) => #t))))

    (define capability (%make-pseudo-capability 'capability (lambda (trace object)
        (check trace (ca object 'capabilities 'contains? equatable 'raw) => #t)
        (check trace (string? (ca object 'name 'raw)) => #t)))))
