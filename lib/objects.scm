(library (lib objects)
    (export ca curried-apply generic-new new store object? cow)
    (import (rnrs) (lib utils))

    (define (curried-apply object . arguments)
        (if (null? arguments)
            object
            (apply curried-apply (object (car arguments)) (cdr arguments))))

    (define ca curried-apply)

    (define (generic-new key? proc)
        (let ((object (store)))
            (lambda (key-or-argument)
                (if (key? key-or-argument)
                    (object key-or-argument)
                    (proc (store) key-or-argument)))))

    (define (new proc)
        (generic-new symbol? proc))

    (define (store)
        (let* ((attributes (mk-map))
               (insert-attribute! (lambda (key value)
                   ; The value is wrapped in a list because it could be #f,
                   ; and #f from map-get means there was no value
                   (set! attributes (map-insert key (list value) attributes)))))
            (lambda (key)
                (let* ((list-wrapped-value (map-get key attributes))
                       (the-value (if list-wrapped-value
                           (car list-wrapped-value)
                           (let ((nested-store (store)))
                               (insert-attribute! key nested-store)
                               nested-store))))
                    (if (object? the-value)
                        (%proxify
                            the-value
                            (lambda (equals-sign-or-argument)
                                (and (eq? equals-sign-or-argument '=)
                                    (lambda (value)
                                        (insert-attribute! key value)
                                        value))))
                        the-value)))))

    (define (object? x)
        (procedure? x))

    (define (%proxify object override-proc)
        ; The DEFINE indirection is useful for converting a proxy to string:
        ; The result will be #<procedure proxy at objects.scm:...>
        (define proxy
            (lambda (x)
                (or (override-proc x)
                    (object x))))
         proxy)

    (define (cow object)
        (let* ((attributes (mk-map))
               (insert-attribute! (lambda (key value)
                   ; The value is wrapped in a list because it could be #f,
                   ; and #f from map-get means there was no value
                   (set! attributes (map-insert key (list value) attributes)))))
            (lambda (key)
                (let* ((list-wrapped-value (map-get key attributes))
                       (the-value (if list-wrapped-value
                           (car list-wrapped-value)
                           (object key))))
                    (if (object? the-value)
                        (%proxify
                            the-value
                            (lambda (equals-sign-or-argument)
                                (and (eq? equals-sign-or-argument '=)
                                    (lambda (value)
                                        (insert-attribute! key value)
                                        value))))
                        the-value))))))
