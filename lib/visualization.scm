(library (lib visualization)
    (export auto-plot-points plot-points)
    (import (rnrs) (lib utils))

    (define (auto-plot-points points)
        (let ((max-x (apply max -1 (map car points)))
              (max-y (apply max -1 (map cdr points))))
            (plot-points (+ 1 max-x) (+ 1 max-y) points)))
    
    (define (plot-points width height points)
        (define (char-at x y)
            (if (member (cons x y) points)
                #\*
                #\space))
                
        (map (lambda (y) (list->string
                (map (lambda (x) (char-at x y)) (range-upper-exclusive 0 width))))
            (range-upper-exclusive 0 height))))
